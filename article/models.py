from django.db import models
from django.utils import timezone


class Article(models.Model):
    title = models.CharField(max_length=50, default="تیتر", verbose_name="تیتر")
    description = models.TextField(max_length=250)
    image = models.ImageField(null=True, blank=True)
    create_at = models.DateTimeField(default=timezone.now)
    